import React, { useEffect, useState }from 'react';
import './nav-bar.css';
import logoImage from './../../../images/main-logo.png';
import SearchInput from '../../../components/home/SearchInput';

const NavBar = () => {
   
    const [windowScroll, setWindowScroll] = useState(window.scrollY);
    const [headerHasBackground, setHeaderHasBackground] = useState(false);

    useEffect(() => {
        if(windowScroll > 0) {
            setHeaderHasBackground(true);
        } else {
            setHeaderHasBackground(false);
        }
    }, [windowScroll]);

    useEffect(()=> {
        window.addEventListener('scroll', () => {
            const currentWindowScroll = window.scrollY;
            setWindowScroll(currentWindowScroll);
        });
    }, []);

    return (
        <header className={`app-header ${headerHasBackground ? 'app-header-background' : ''}`}>
            <nav className="app-nav">
                <a href="/home" className="nav-logo">
                    <img src = {logoImage}/>
                </a>
                <ul className="nav-links-list">
                    <li>
                        <SearchInput />
                    </li>
                    <li className="nav-content-link">
                        <a  href="#">Contents <i className="fa fa-chevron-down"></i></a>
                        <ul className="content-links-list">
                            <li><a href="#">Saglikli yasam</a></li>
                            <li><a href="#">Spor Istanbul</a></li>
                            <li><a href="#">Sinema sever2</a></li>
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Oscar Movies</a></li>
                            <li><a href="#">Fantastic</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i className="fa fa-bars"></i>My list</a>
                    </li>
                    <li>
                        <a className="nav-user-name" href="#">Name</a>
                    </li>
                </ul>
            </nav>
            <div className="header-static-wrapper">
                <div className="header-static-part">Let's Watch?</div>
            </div>
        </header>
    );
};

export default NavBar;