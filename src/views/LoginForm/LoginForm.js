import './login-form.css';
import React from 'react';
import { useHistory } from "react-router-dom";

const LoginForm = () => {
    const history = useHistory();

    const onButtonClick = () => {
        history.push('/home');
    };

    return (
        <div className="login-page">
            <div className="form-wrapper">
                <form action="#" method="POST">
                    <div className="form-group">
                        <label htmlFor="user">Username</label>
                        <input type="text" id="user" name="user" className="form-user" placeholder="Enter Username" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" id="password" name="password" className="form-password" placeholder="Enter Password" />
                    </div>

                    <button type="submit" className="form-button" onClick={onButtonClick}>
                        Login
                    </button>

                    <div className="form-footer">
                        <span>Not registered?</span>
                        <a href="#">Create an account</a>
                    </div>
                </form>
            </div>            
        </div>
       
    )
}

export default LoginForm;