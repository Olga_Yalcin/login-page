import './styles/Main.css';
import React from 'react';
import LoginForm from './views/LoginForm/LoginForm';
import Home from './components/home/Home';
import { BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import VodDetail from './components/vodDetail/VodDetail';
import NavBar from './views/home/navBar/NavBar';


const App = () => {

    return (
        <div className="app-wrapper">
            <Router>
            <Switch>
                <Redirect from='/' exact to="/login"/>
                <Route path="/login" component={LoginForm} />
                <Route path="/home" exact component={Home} />
                <Route path="/home/:id"  component={VodDetail} />                   
            </Switch>
            </Router>  
        </div>
    )
}

export default App;