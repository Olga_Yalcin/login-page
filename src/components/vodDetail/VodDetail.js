import React, {useEffect, useState} from 'react';
import moviesSearch from './../../apis/getMoviesData';
import './VodDetail.css';
import VideoPlayer from '../videoPlayer/VideoPlayer';
import videoSearch from './../../apis/vodApi';
import NavBar from './../../views/home/navBar/NavBar';

const VodDetailInfo = ({movieData}) => {
    const { genres, title, overview, production_countries: prodCountries, release_date : releaseDate, vote_average: vote} = movieData;
   
    const genresList = genres ? genres.map(genre => <span key={genre.name}>{genre.name}</span>) : null; 
    const prodCountriesList = prodCountries ? prodCountries.map(country => <span key={country.name}>{country.name}</span>) : null;

    return (
        <div className="vod-detail-info">
            <h3 className="vod-detail-title">{title}</h3>
            <div className="vod-detail-genres">{genresList}</div>
            <div className="vod-detail-overview">{overview}</div>
            <div className="vod-detail-item"><span>Production Countries: </span>{prodCountriesList}</div>
            <div className="vod-detail-item"><span>Release date: </span>{releaseDate}</div>
            <div className="vod-detail-item"><span>Rating: </span>{vote} <i className="fa fa-star"></i></div>
        </div>
    )
}

const VodDetail = ({match}) => {
    const [movieData, setMovieData] = useState({});
    const [movieVideo, setMovieVideo] = useState('');

    const findMovie = async () => {
      const response = await moviesSearch.get(`/movie/${match.params.id}`, {
        params: {
          api_key: '77a49e4ff6ec93e01b8b3c92f2ed5f57'
        }
      });

      if(response.data) {
        setMovieData(response.data);
      }
    };

    const findVideo = async () => {
        const response = await videoSearch.get('/search', {
            params: {
                query: 'nature'
            },
            
            headers: {
                "Authorization": "563492ad6f91700001000001de056845e84d4a59972b113e4ca9e2c3"
            }
        });
  
        if(response.data) {
          const videoNumber = Math.floor(Math.random() * 14);
          console.log(videoNumber);
          const videoSource = response.data.videos[videoNumber].video_files[0].link;
          
          setMovieVideo(videoSource);
        }
      };
       
    useEffect(() => {
        findVideo();
        findMovie();
    }, []);

    return (
        <div className="vod-detail-wrapper">
            <NavBar />
            <VodDetailInfo movieData={movieData}/>
            {movieVideo ? <VideoPlayer movieVideo={movieVideo}/> : null}
        </div>
    )
}

export default VodDetail;