import React, { Component } from 'react';
import Slider from "react-slick";
import './bannerSlider.css';

const banners = [
    {    
        id: 1,
        title: 'Mortal Engines',
        description: 'In a post-apocalyptic world where cities ride on wheels and consume each other to survive, two people meet in London and try to stop a conspiracy.',
        duration: '122 dk',
        year: '2019',
        genre: 'Dram - Suç - Gerilim',
        poster: 'http://demo.atlastv.net/contents/4259B1DC-3B90-431F-ACA2-14A374F2299C/poster-Web-MainBoard-Poster-1592416966.jpg'
    },
    {    
        id: 2,
        title: 'Iron Man',
        description: "When Tony Stark's world is torn apart by a formidable terrorist called the Mandarin, he starts and odyssey of rebuilding and retribution.",
        duration: '122 dk',
        year: '2019',
        genre: 'Dram - Suç - Gerilim',
        poster: 'http://demo.atlastv.net/contents/EC6F416B-6320-4EFB-A9B5-7620728C2D42/comp-poster-mainboard-1575989729.jpg'
    },
    {    
        id: 1,
        title: 'Terminator Genisys',
        description: "Yıl 2029... İnsanların direniş önderi John Connor, makineler imparatorluğu Skynet'in ölümcül gücüne karşı savaşmaktadır.",
        duration: '122 dk',
        year: '2019',
        genre: 'Dram - Suç - Gerilim',
        poster: 'http://demo.atlastv.net/contents/6B3FBDD5-0D14-467A-844B-442E9C5ECE1F/comp-poster-mainboard-1579187655.jpg'
    },
    {    
        id: 1,
        title: 'Terminator: Dark Fate',
        description: 'An augmented human and Sarah Connor must stop an advanced liquid Terminator, from hunting down a young girl whose fate is critical to the human race.',
        duration: '122 dk',
        year: '2019',
        genre: 'Dram - Suç - Gerilim',
        poster: 'http://demo.atlastv.net/contents/4168F7C3-C8FA-440A-A498-6067BA8C4862/comp-poster-mainboard-1575990723.jpg'
    },
];

class BannerSlider extends Component {
    render() {
      const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        className: 'banner-slider'
      };

      return (
          <Slider {...settings}>

            {banners.map((slide, index) => {
                return (
                    <div className="banner-wrapper" key={index}>
                        <img className="banner-image" src={slide.poster} alt={slide.poster}/>
                        <div className="banner-info">
                            <h2 className="banner-title">{slide.title}</h2>
                            <div className="banner-description">{slide.description}</div>                            
                        </div>
                    </div>
                ) 
            })}

          </Slider>
      );
    }
}

export default BannerSlider;


