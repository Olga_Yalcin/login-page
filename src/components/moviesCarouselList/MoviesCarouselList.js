import React from 'react';
import CarouselSlider from '../carouselSlider/CarouselSlider';

const genresId = [
    {
        genre: "Comedy",
        id: 35
    },
    {
        genre: "Action",
        id: 28
    },
    {
        genre: "Family",
        id: 10751
    },
    {
        genre: "Horror",
        id: 27
    },
    {
        genre: "Adventure",
        id: 12
    },
    {
        genre: "Crime",
        id: 80
    },
    {
        genre: "Fantastic",
        id: 14
    }
];

const MoviesCarouselList = () => {
    const moviesList = genresId.map(({genre, id}) => {
        return <CarouselSlider key={id} movieGenre={genre} movieGenreId={id} />
    });

    return (
        <div className="movies-carousel-list">
            {moviesList}
        </div>
    )
};
 
export default MoviesCarouselList;