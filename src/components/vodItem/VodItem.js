import React from 'react';
import { useHistory } from "react-router-dom";

const VodDetail = (props) => {

    const {backdrop_path, title, release_date} = props;

    const history = useHistory();

    const onMovieItemClick = (movieInfo) => {
        history.push(`/home/${movieInfo.id}`);
    };

    return (
        <div onClick={() => onMovieItemClick(props)} className="carousel-item-wrapper">
            <img className="carousel-item-image" src={`https://image.tmdb.org/t/p/original/${backdrop_path}`} alt={title}/>
            <div className="carousel-item-info">
                <h2 className="carousel-item-title">{title}</h2>
                <div className="carousel-item-date">{release_date}</div>                            
            </div>
        </div>
    )
}

export default VodDetail;