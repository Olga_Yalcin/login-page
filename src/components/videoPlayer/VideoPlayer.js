import React, {useEffect, useRef} from 'react';
import videojs from 'video.js';
import 'video.js/dist/video-js.css';

const VideoPlayer = ({movieVideo}) => {

    const videoRef = useRef();

    const videoOptions = {
        autoplay: 'muted',
        controls: true,
        height: 600,
        width: 800,
        sources: [{
          src: movieVideo,
          type: 'video/mp4'
        }]
    };

    useEffect(() => {  
        const player = videojs(videoRef.current, videoOptions, () => {
        });

        return () => {
        player.dispose()
        };
        
    }, []);
 
    return (
      <div>	
        <div data-vjs-player>
          <video ref={videoRef} className="video-js"></video>
        </div>
      </div>
    )
}

export default VideoPlayer;