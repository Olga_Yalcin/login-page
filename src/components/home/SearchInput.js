import React, {useState, useEffect, useRef, Fragment} from 'react';

const SearchInput = () => {
    const [searchAvailability, setSearchAvailability] = useState(false);
    const ref = useRef();

    useEffect(() => {
        const onBodyClick = (e) => {
            if(ref.current && ref.current.contains(e.target)) {
                return;
            }
            setSearchAvailability(false);
        };

        document.body.addEventListener('click', onBodyClick, {capture: true});

        return () => {
            document.body.removeEventListener('click', onBodyClick);
        };

    }, []);

    const openInput = () => {
        if(searchAvailability === false) {
            setSearchAvailability(true);
        }
    };

    return (
        <Fragment>
            <input ref={ref}  onClick={openInput} className={`nav-search-input ${searchAvailability ? 'opened-input' : ''}`} type="text" />
        </Fragment>
    )
}

export default SearchInput;