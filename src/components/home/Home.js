import React, {Fragment} from 'react';
import NavBar from '../../views/home/navBar/NavBar';
import BannerSlider from './../bannerSlider/BannerSlider';
import MoviesCarouselList from '../moviesCarouselList/MoviesCarouselList';
//import { BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';



const Home = () => {
    return (
        <Fragment>
                <NavBar/>
                <BannerSlider/>
                <MoviesCarouselList/> 
        </Fragment>
    )
}

export default Home;