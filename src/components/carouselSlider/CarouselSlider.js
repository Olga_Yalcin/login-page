import React, {useState, useEffect} from 'react';
import Slider from "react-slick";
import './carouselSlider.css';
import moviesSearch from './../../apis/getMoviesData';
import VodItem from './../vodItem/VodItem';

const CarouselSlider = ({movieGenre, movieGenreId}) => {

    const [moviesData, setMoviesData] = useState([]);

    const searchMovies = async genre => {
      const response = await moviesSearch.get("/discover/movie", {
        params: {
          with_genres: genre,
          api_key: '77a49e4ff6ec93e01b8b3c92f2ed5f57'
        }
      });

      if(response.data.results) {
        setMoviesData(response.data.results);
      }
    };
        
    useEffect(() => {
        searchMovies(movieGenreId);
    }, []);

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        className: 'carousel-slider',
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                initialSlide: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
    };

    return (
        <div className="carousel-slider-wrapper">
            <h2 className="carousel-slider-title">{movieGenre}</h2>
            <Slider {...settings}>
                
            {moviesData.map((movieInfo) => {
                return (
                  <VodItem key={movieInfo.id} {...movieInfo} />
                ) 
            })}

            </Slider>              
        </div>
    )
}

export default CarouselSlider;

